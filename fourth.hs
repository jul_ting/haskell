-- pyth n =[ (x,y,z) |x<-[1..n],y<-[1..n],z<-[1..n],x^2+y^2==z^2,x<y]
-- pytho 0 = return ()
-- pytho x = do 
--     print(x)
--     -- if x == 1 then True else False 
--     pytho (x-1)
    

--list comprehension
--[output|condition]

--display all even numbers from 1 to n

evens n = [x |x<-[1..n],mod x 2==0]

--display all even numbers from 1 to n that 
--are both divisible by 2 and 3

evens2 n =[x |x<-[1..n],mod x 2==0, mod x 3==0]

--display all even numbers from 1 to n that 
--are both divisible by 2 or 3

evens3 n =[x |x<-[1..n],or[mod x 2==0, mod x 3==0]]

--given a list of values, display only the even numbers
--eg dispevens [3,6,5,8] =>[6,8]
dispevens a = [x |x<-a,mod x 2==0]

--display all the perfect divisors of a number n
--eg: divisors 6 => [1,2,3,6]
divisors a = [x |x<-[1..a],mod a x==0]
--divisors a = [x |x<-[1..a], a `rem` x==0]

--for a given number n display true if it is prime
-- and false if non-prime
--eg: isprime 7=>True
isprime a = length (divisors 2)==2

isprime2 n =divisors n ==[1,n]

--display a list of all prime numbers from 1 to n.
--eg: disprimes 10=>[2,3,5,7]
disprimes a = [x |x<-[1..a],isprime2 x]

--display the cubes of all numbers from m to n.
--eg cubes 2 5=>[8,27,125] 
cubes a b=[x^3 |x<-[a..b]]

--given a list of numbers, display the sum of positive value ONLY
--eg: sumpos [1,-3,4,-5]=>5

sumpos a = sum [x |x<-a,x>=0]

--given a list of values display True if all values are prime and 
--false otherwise. Eg: areprimes [2,7,13,17]=> True

areprimes a = length [x |x<-a,isprime2 x] == length a
areprimes2 a = and [isprime2 x |x<-a]

--given a list of values. Multiply each value with its index 
--position starting from 1.
--Eg: fun [4,8,0,1] =>[4,16,0,4]
--cant do

--Tuples
sample =[ (x,y)|x<-[1..3],y<-[5..7]]
--nested for

--given two lists. Display the product terms of every value in the
--first list with that of the second list
--eg prodlists [1,2] [2,3,4]=>[2,3,4,4,6,8]
prodlists a b=[ x*y |x<-a,y<-b]

--display all pythagorean triplets from 1 to n
--eg: pyth 25
--[(4,3,5),(8,6,10),(12,5,13),(12,9,15),(15,8,17),(16,12,20)
--,(20,15,25)]
pyth n =[ (x,y,z) |x<-[1..n],y<-[1..n],z<-[1..n],x^2+y^2==z^2,x<y]

--curried expressions