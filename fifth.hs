{-
Given a list of tuples display the id, length of the name and marks >= 50
Eg: sortout [(1, "sam", 98), (2, "alex", 45), (3, "maria", 78)] => [(1,3,98), (3,5,78)]
-}

list1 = [(1, "sam", 98), (2, "alex", 45), (3, "maria", 78)]
sortout list = [(a,b,c) | (a, b, c) <- list, c >= 50 ]

-- listout [(2,5), (7,2), (3,6)]=> [(4,25), (9,36)]
list2 = [(2,5), (7,2), (3,6)]
listout list = [(a^2,b^2) | (a,b) <- list, a < b]

-- Write a function that displays the first element in a list
-- don't use head

myhead (a:b) = a

-- Write a function that displays all the elements in a list except first
-- don't use tail

mytail (a:b) = b

factorial 0 = 0
factorial 1 = 1
factorial x = x * factorial (x-1)

mylast [n] = [n]
mylast (a:b) = mylast b

fib 1 = 0
fib 2 = 1
fib n = fib (n-1) + fib (n-2)

