-- Recursive
meven 0 = True
meven 1 = False
meven n = modd(n-1)
modd n = meven(n-1)

-- Write a function to add all numbers in a list
-- don't use sum function in your answer

addall [] = 0
addall (a:b) = a + addall b


mylast [n] = n
mylast (a:b) = mylast b

-- Rewrite the init function
myinit [x] = []
myinit (a:b) = [a] ++ myinit b

minit (a:b) = if (length b > 0) then (a:minit b) else []

-- Display the nth element in a list
-- display 3 [4,3,5,6,7,8] => 5

disp 1 (x:xs) = x
disp n (x:xs) = disp (n-1) xs

-- nthof a b = if (a > 1) then nthof (a-1) (tail b) else (head b)

-- Display the largest value in a list

largest [n] = n
largest (a:b) = if(a > head(b)) then largest (a:(tail b)) else largest ((head b):(tail b))


larg [n] = n
larg (a:b:c) = if (a > b) then larg (a:c) else larg (b:c)

-- reverse a list of values
rev [a] = [a]
rev (a:b) = rev b ++ [a]

-- check if a list is a palindrome
pal xs = xs==(rev xs)

-- given a list, determine if it is sorted in the ascending order

issorted [] = True
issorted [n] = True
issorted (a:b:c) = if (a <= b) then issorted ([b]++c) else False

-- zip
myzip [] [] = []
myzip [] n = []
myzip n [] = []

myzip (a:as) (b:bs) = ((a, b):myzip as bs)

-- quick sort, merge sort NEXT