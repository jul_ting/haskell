-- main = putStrLn "Hello, World!"
-- main = print (3+5^189*1678)

{-
alphanumeric
even odd sqrt div sum truncate round ceiling floor mod
sin cos tan 
asin acos atan
sinh tanh cosh 
asinh atanh acosh

symbolic = + - * / ^ ++ !! **

sum [1,2,3]

infix 
3 + 5 
4 `div` 2

prefix
+ 3 5 
div 4 2

postfix (?)
3 5 + 

num9 = 9 :: Int
sqrt (fromIntegral num9)

Creating a list
1 : 2 : 3 : []
[1,2,3]

LISTS
length [1,2,3]
reverse 
null
head
last
init
tail
take x []
drop x []

elem 7 []
7 `elem` []
maximum
minimum
product []
evenList = [2,4..20]
['a'..'z']
sort

[1..10] !! 3 
will return 4










-}




-- for 3 + 5 * 4
-- main = print((+) 3 ((*) 4 5))

-- 2 ^ 7 - 4
main = print((-) ((^) 2 7) 4)
