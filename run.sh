#!/bin/sh

# ghci to run on terminal
# :load <module>
# :r to refresh
ghc -o ${1} ${1}.hs
./${1}
