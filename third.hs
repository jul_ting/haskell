-- isbigger :: Integer -> Integer -> String
-- isbigger x y = if x > y then show x ++ " is bigger" else show y ++ " is bigger"
-- abc x y = if x > y then show x else "null"

isbigger x y = if (x > y) then x else y

largestof a b c = if a > b && a > c then a else if b > c then b else c

{--
    1. read 3 numbers and determine if they are pythagorean triplets
    2. display the absolute value of a number
    3. for given two numbers determine if they divide each other perfectly
    4. Display the sinc of a number. sinc x = sin x / x. but sinc 0 is 1
    5. read a number n. determine if it is armstrong number
--}

pytho a b c = if a**2 + b **2 == c**2 then True else if c**2 + b **2 == a**2 then True else if a**2 + c **2 == b**2 then True else False
abso x = if x < 0 then (-x) else x
perfdiv x y = if (mod x y == 0) then True else if (mod y x == 0) then True else False
sinc x = if x ==0 then 1 else sin x/x
getdigits a = (div a 100) : (mod(div a 10) 10) : (mod a 10) : []
as1 x = if ((getdigits x !! 0)^3 + (getdigits x !! 2)^3 + (getdigits x !! 1)^3) == x then True else False
as2 x = if ((div x 100))^3 + (((div x 10) - ((div x 100)* 10)))^3 + ((mod x 10))^3 == x then True else False
as3 x = if ((div x 100))^3 + (mod(div x 10) 10)^3 + ((mod x 10))^3 == x then True else False

bmiTell :: (RealFloat a) => a -> String  
bmiTell bmi  
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  